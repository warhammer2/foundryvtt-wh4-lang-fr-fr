
package.path = package.path .. ";luajson/?.lua"
local JSON = require"json"

local oldf = "../compendium/wfrp4e-core.criticals.json.SAVED"
local newf = "../compendium/wfrp4e-core.criticals.json"

local origin = io.open(oldf, "r")
local strjson = origin:read("*a")
origin:close()
local oldjson = JSON.decode(strjson)

local new = io.open(newf, "r")
strjson = new:read("*a")
new:close()
local newjson = JSON.decode(strjson)

for _, newItem in pairs(newjson.entries) do 
  for _, oldItem in pairs(oldjson.entries) do 
    if newItem.name == oldItem.name then 
      newItem.id = oldItem.id
    end
  end
end

local jsonout = JSON.encode( newjson ) 
local fout = io.open("output.json", "w+")
fout:write( jsonout )
fout:close()
