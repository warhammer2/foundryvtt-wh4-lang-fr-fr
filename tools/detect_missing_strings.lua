package.path = package.path .. ";luajson/?.lua"
local JSON = require"json"

local enjsonf = "../../WFRP4e-FoundryVTT/lang/en.json"
local frjsonf = "../fr.json"

local fp = io.open(enjsonf, "r")
local entags = JSON.decode( fp:read("*a") )
fp:close()

fp = io.open(frjsonf, "r")
local frtags = JSON.decode( fp:read("*a") )
fp:close()
        
for tag, value in pairs(entags) do
  if not frtags[tag] then 
    print(tag, value)
  end
end
                                    

