# FoundryVTT - WH4-fr-FR

**DISCLAIMER/LICENCE**

This module depends on the WFRP4 system from MooMan and require the official C7 'Warhammer Core' module from C7.
This module has been declared at C7 and discussions are ongoing to get the licence agreement from the French Company Khaos Project.
The present works is under the intellectual property of Khaos Project and C7 only.

This module is the French translation of the WH4 system from Moo Man.

It aims to translate all text, including : 
 * Interface and sheets
 * Tables
 * Compendium

## Installation

Pour l'installer, suivre ces instructions : 

1.  Au sein de Foundry, allez dans le menu "Configuration and Setup", puis sur l'onglet "Modules"
2.  Installez le module de françisation de l'interface principale : https://gitlab.com/baktov.sugar/foundryvtt-lang-fr-fr/raw/master/fr-FR/module.json
3.  Installez le module 'babele'  : https://gitlab.com/riccisi/foundryvtt-babele/raw/master/module/module.json
4.  Installez ce module WH4-fr : https://gitlab.com/LeRatierBretonnien/foundryvtt-wh4-lang-fr-fr/-/raw/master/module.json

[IN FRENCH ONLY BELOW]

## Usage Instructions

Il n'y a pas de règles particulières, il faut juste positionner la langue à "Français" dans les réglages du système.

## Contribuer

Pour contribuer, vous pouvez me joindre sur FounddryVTT FR : https://discord.gg/pPSDNJk , ou bien sur le Discord de FoundryVTT (EN), 
 
Pseudo : LeRatierBretonnien

## Statut

**Au 06/06/2020**

* Foundry v0.6.0+
* Warhammer v4 système v1.5.0+
* Babele v1.19+
* Warhammer v4 traduction FR v0.81+

Pour info, il existe énormément de cas particuliers dans le système de règles, donc il est possble qu'il reste des défauts, soit dans le module de traduction, soit dans le système lui-même. 
Pournous remonter des bugs, assurez vous d'être dans les versions cités ci-dessus. Merci :) !

**Fait**

* fr.json -> Celewyr (FB/Discord)
* compendium/bestiary -> Louis Lebeke (FB)
* tables/delirium -> Louis Lebeke (FB)
* tables/eyes-hair -> LeRatierBretonnien
* tables/career-critarm-critbody-crithead-critleg-hitloc-mutemental-mutephys-oops-species-talents- -> Xav Tuckel
* compendium/traits -> Moilu (Discord),  B. Migneaux (FB)
* compendium/criticals -> Gharazel (Discord)
* compendium/careers -> LeRatierBretonnien 
* tables/wrath -> Jerome Paire (FB)
* compendium/prayers -> LeRatierBretonnien
* tables/travel -> LeRatierBretonnien
* compendium/injuries -> Baptiste Delanoue (FB)
* compendium/trappings -> LeRatierBretonnien
* tables/minormis -> Baptiste Delanoue (FB)
* tables/doom -> LeRatierBretonnien
* compendium/diseases -> LeRatierBretonnien
* compendium/mutations -> LeRatierBretonnien
* compendium/psychologies -> Moilu (Discord),  B. Migneaux (FB)
* tables/majormis -> Baptiste Delanoue (FB)
* Travaux de traductions automatiques des compendiums interdépendants (ie carrières qui pointe vers skills/trappings/talents)
* tables/event -> Celewyr (FB/Discord)
* compendium/spells -> Gharazel (Discord)
* compendium->diseases -> Gharazel (Discord)
* carrers -> traduction des équipements de base
* compendium/skills -> Dr. Droide (Discord) 
* compendium/talents ->  LeRatier/Dr.Droide/Moilu/Gharazel

## Compatibility

Foundry : 0.5.4+
Warhammer v4 : v1.2+
Babele : 1.9+

## Feedback

Every suggestions/feedback are appreciated, if so, please contact me on discord (LeRatierBretonnien) or insert bug here in the Gitlab interface.

## Acknowledgments

* Thanks to Moo Man for his great work in the module creation an design
* Merci à @Tuckel pour la traduction initiale des textes principaux, des tables et du compendium
* Merci à tous les traducteurs : Celewyr, Moilu, Gharazel, Jerome paire, Baptiste Delanoue, Dr. Droide, Gharazel

## License

WH4-fr-FR is a module for Foundry VTT by LeRatierBretonnien and is licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).

This work is licensed under Foundry Virtual Tabletop [EULA - Limited License Agreement for module development v 0.1.6](http://foundryvtt.com/pages/license.html).

Le système Warhammer 4 et tout ses contenus sont la propriété de Cubicle7, sous Licence de Games Workshop, et tout les droits leur reviennent naturellement. 
La traduction française de Khaos Project qui a servi de base à cette traduction, est à créditer pour tout les termes et items traduits, ce qui reste leur entière propriété, naturellement également.

All original material belongs to the aforementioned authors. No infringement intended.

This module is completely unofficial and in no way endorsed by Games Workshop Limited. Chaos, the Chaos device, the Chaos logo, Citadel, Citadel Device, Darkblade, the Double-Headed/ Imperial Eagle device, ‘Eavy Metal, Forge World, Games Workshop, Games Workshop logo, Golden Demon, Great Unclean One, GW, the Hammer of Sigmar logo, Horned Rat logo, Keeper of Secrets, Khemri, Khorne, the Khorne logo, Lord of Change, Nurgle, the Nurgle logo, Skaven, the Skaven symbol devices, Slaanesh, the Slaanesh logo, Tomb Kings, Trio of Warriors, Twin Tailed Comet Logo, Tzeentch, the Tzeentch logo, Warhammer, Warhammer Online, Warhammer World logo, White Dwarf, the White Dwarf logo, and all associated marks, names, races, race insignia, characters, vehicles, locations, units, illustrations and images from the Warhammer world are either ®, TM and/or © Copyright Games Workshop Ltd 2000-2020, variably registered in the UK and other countries around the world. Used without permission. No challenge to their status intended. All Rights Reserved to their respective owners.
